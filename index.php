<?php
function getPercent()
{
    $paid = 130;
    $totalWidth = 700;

    $resp = round(($paid / $totalWidth) * 100, 2);

    return $resp;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Jua" rel="stylesheet">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RASPINATION2018</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/progressBar.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64487887-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-64487887-1');
    </script>
    <!-- Custom styles for this template -->
    <link href="css/half-slider.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-light ">
    <div class="container">
        <a class="navbar-brand" href="#" style="font-family: 'Jua'"><b><h2>Raspination2018</h2></b></a>

    </div>
</nav>

<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="15000">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url(images/1.png)">
                <div class="carousel-caption">
                    <h4 style="color: black;">Give Roman an awesome birthday present</h4>
                    <p style="color: black;">As I get older as childish I get. This year I'm crowfunding myself an
                        Oculus Rift Virtual Reality Set, I've been dreaming about for some time now. I've already build
                        a computer for it, but the set itself is quite pricey and it would be awesome if you could help
                        me out with a small amount as present for my birthday!</p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url(images/2.png)">
                <div class="carousel-caption">
                    <h4 style="color: black;">Подарите Ромке подарок на день рождения</h4>
                    <p style="color: black;">Чем взрослее я становлюсь, тем более чувствую себя ребенком. В этом году я
                        решил собрать денег на Очки Виртуальной реальности Oculus Rift, о которых давненько мечтал. Я
                        уже собрал под него компьютер, но сами очки, очень не дешево стоят и это было бы здорово, если
                        бы вы выручили меня небольшой суммой, в подарок на моё день рождения!</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url(images/3.png)">
                <div class="carousel-caption">
                    <h4 style="color: black;">תנו לרומן מתנת יום הולדת מהממת</h4>
                    <p style="color: black;">ככל שאני מתבגר אני מרגיש יותר ילדותי. השנה אני אוסף כסף למערכת מציאות מדומה
                        ויהיה מעולה עם תוכלו לעזור לי עם מתנת יום הולדת מהממת</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<!-- Page Content -->
<section class="py-5">
    <div class="container">

        <!------ Include the above in your HEAD tag ---------->

        <div class="container">
            <div class="row">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo(getPercent()); ?>"
                         aria-valuemin="0"
                         aria-valuemax="100" style="width: <?php echo(getPercent()); ?>%;">
                        <span class="sr-only"><?php echo(getPercent()); ?>% Complete</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col"><b><?php echo(getPercent()); ?>%</b><br/>
                        <small>FUNDED</small>
                    </div>
                    <div class="col"><b>$130</b><br/>
                        <small>PLEDGED</small>
                    </div>
                    <div class="col"><b>
                            <?php
                            $now = time(); // or your date as well
                            $your_date = strtotime("2018-07-13");
                            $datediff = $your_date - $now;

                            echo round($datediff / (60 * 60 * 24));
                            ?>
                        </b><br/>
                        <small>DAYS</small>
                    </div>
                    <div class="col"><b>3</b><br/>
                        <small>Presents</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <center><h1>You can give a present using one of these systems</h1>
        <div>
            <a href="http://hey.pbme.co/LbSW6q"><img src="images/paybox.png" height="180"></a>
            <a href="http://paypal.me/Raspination2018"><img src="images/paypal.png" height="180"></a>
            <a href="https://www.amazon.com/Amazon-Amazon-com-eGift-Cards/dp/BT00DC6QU4"><img
                        src="images/amazon.png" height="170"> </a>
        </div>
        <br>
        <div class="alert alert-info">
            <strong>Info!</strong> If you choose Amazon GiftCard, please send it to:
            <strong>raspik@gmail.com</strong>
        </div>
    </center>
    </div>
</section>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white"> &copy; Roman Raspik Chukhvanov</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
